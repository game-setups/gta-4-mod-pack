using System;
using System.Windows.Forms;
using GTA;

public class RegenScript : Script 
{
	// Math.Clamp function
	public static int Clamp(int val, int min, int max) {
		return (val < min) ? min : (val > max) ? max : val;
	}
	
	
	// Global variables
	const int MAX_HEALTH = 100;
	const int iHealOnRegen = 5; // Health (percent) given on regen
	const int iTimeBetweenHeals = 5000; // Time between regen (milliseconds)
	
	public RegenScript()
	{
		Interval = iTimeBetweenHeals; // Script tick rate
		this.Tick += new EventHandler(this.IncreaseHealth);
	}

	// Func to give player a health boost if not recently injured
	int PlyLastHealth = 0;
	private void IncreaseHealth(object sender, EventArgs e) {
		if ((Player.Character.Health == PlyLastHealth) && (Player.Character.Health < MAX_HEALTH) && (Player.Character.Health > 0)) {
			Player.Character.Health = Clamp(Player.Character.Health + iHealOnRegen, 0, MAX_HEALTH);
		}
		
		PlyLastHealth = Player.Character.Health; // Flag to block next regen if the player is injured between regenerations (stops heals happening straight after dmg being taken)
	}
}