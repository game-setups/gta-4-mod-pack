# Description
My personal collection of my favourite community mods. My goal was to make this as optimised as possible, with almost no impact on performance.

## Installation
1. If any mods were installed prior, delete your entire game directory and redownload/reinstall before applying
2. Install 1.0.7.0 patch & GFWL market place from Prerequisites folder!
3. Install into game directory, replacing over existing files

## Suggestions
- Disable Clip capture to reduce CPU load
- Bump saturation slightly in display settings
- If you have over 4GB of RAM, edit `pc > stream.ini` (in your game directory). Change all values reading `204800` to: `409600`.

## File purposes
### Vehicles
`common > data > handling.dat`				- Increased vehicle damage on impact and realistic handling	<br />
`pc > data > filelist.pak`					- Allows handling.dat overwrite	<br />
`common > data > carcols.dat`				- Better taxi colour <br />

### Loading screens
`common > data > loadingscreens_pc.dat` 	- Skips splashscreen (intro logos)	<br />
`pc > textures > loadingscreens.wtd` 		- Colours the loading screen backgrounds	<br />

### Graphics
`pc > textures > hud.wtd`					- HD sniper scope textures	<br />
`pc > textures > lights_occluders.wtd`		- HD car under-shadow	<br />
`pc > data > timecyc.dat`					- Timecycle files remove ugly yellow tint, fixes lighting, wasted/busted filter, colour and fog effects	<br />
`pc > data > timecyclemodifiers.dat`	<br />
`pc > data > timecyclemodifiers2.dat`	<br />
`pc > data > timecyclemodifiers3.dat`	<br />
`pc > data > timecyclemodifiers4.dat`	<br />

### Scripts
`Trainer.asi`								- Trainer/cheat menu	<br />
`trainer.ini`								- Trainer settings	<br />
`dsound.dll`								- Loads C# and C++ scripts in scripts folder		<br />
`scripts > regen.cs`						- Script to regenerate health over time when not damaged<br />
`ScriptHook.dll`							- ASI loader	<br />
`ScriptHookDotNet.asi`						- ASI loader	<br />
`commandline.txt`							- Launch parameters to fix modern GPUs showing as having no RAM and other optimisations	<br />

### Miscellaneous
`common > data > WeaponInfo.xml`			- Increased range of most weapons, all weapon fire rates match blindfire rate, increased fire rate of M4	<br />
`pc > models > weapons.img`					- Coloured weapon icons	<br />

## Credits
| Mod      | Author   |
| -------- | -------- |
|Better handling mod						| [Killatomate](https://www.gtainside.com/en/gta4/mods/28015-realistic-driving-n-flying-2-7/)    |
|Skip loading screens						| [Meax](https://www.gtainside.com/en/gta4/mods/27328-skip-loading-screens/)    | 
|Coloured loading screens					| [Mitchell Maniac](https://gta4-mods.com/new-gta-iv-loading-screens-f2338.html)    | 
|HD scopeps									| [RollY](https://www.gtainside.com/en/gta4/mods/28409-new-scope-crosshair/)    | 
|HD car under-shadow						| [grandtheftauto1233](https://www.gtagarage.com/mods/show.php?id=8245)    | 
|Improved timecycle							| [DKT70](https://gta4-mods.com/realityiv-2-0-f25401.html)    | 
|Simple native trainer						| [Sjaak32](https://www.gtainside.com/en/gta4/trainers/36723-gta-iv-eflc-simple-native-trainer-v6-3)    | 
|DSound										| [dev-c](http://www.dev-c.com/gtaiv/asiloader/)    | 
|regen.cs 									| [CardboardFace (me)](https://gitlab.com/CardboardFace/gtaiv-health-regen-mod)    | 
|ScriptHook & ScriptHookDotNet				| [HazardX](http://hazardx.com/files/gta4_net_scripthook-83)    | 
|commandline.txt 							| CardboardFace (me)    |